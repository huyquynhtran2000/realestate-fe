import Sidebar from "./components/sidebar/Sidebar";
import Topbar from "./components/topbar/Topbar";
import "./App.css";
import Home from "./pages/home/Home";
import { React, useEffect, createContext, useReducer,useContext } from 'react'
import { BrowserRouter, Route, Switch, useHistory } from 'react-router-dom'
import UserList from "./pages/userList/UserList";
import User from "./pages/user/User";
import NewUser from "./pages/newUser/NewUser";
import ProductList from "./pages/productList/ProductList";
import Product from "./pages/product/Product";
import ProductDetail from "./pages/productDetail/ProductDetail";
import PostList from "./pages/postList/PostList";
import { reducer, initialState } from './reducers/dataReducer'

const UserContext = createContext()

export {UserContext}

const Routing = () => {
  const history = useHistory()
  const { state1, dispatch1 } = useContext(UserContext)
  const { state, dispatch } = useContext(UserContext)

  
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"))
    console.log(user)
    dispatch1({type:"USER",payload:user})
  }, [])

  useEffect(() => {
    const data = [localStorage.getItem("address"),localStorage.getItem("category"), localStorage.getItem("price_min"), localStorage.getItem("price_max")]
    console.log(data)
    dispatch({type:"DATA",payload:data})
    
  }, [])
  
  return (
    <Switch>
        <Route exact path="/admin">
            <Home />
        </Route>
        <Route exact path="/admin/users">
            <UserList />
        </Route>
        <Route exact path="/admin/user/:userId">
            <User />
        </Route>
        <Route exact path="/admin/products">
            <ProductList />
        </Route>
        <Route exact path="/admin/product/:productId">
            <ProductDetail />
        </Route>
        <Route exact path="/admin/postNews">
            <PostList />
        </Route>
    </Switch>
)
}

function Admin() {
    const [state, dispatch] = useReducer(reducer,initialState)
    const [state1, dispatch1] = useReducer(reducer,initialState)
    return (
        <UserContext.Provider value={{ state, dispatch, state1, dispatch1}}>
            <BrowserRouter>
                <Topbar />
                <div className="container-admin">
                    <Sidebar />
                    <Routing></Routing>
                </div>
            </BrowserRouter>
        </UserContext.Provider>
        
  );
}

export default Admin;