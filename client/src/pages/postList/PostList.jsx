import "./postList.css";
import "antd/dist/antd.css";
import { Pagination, Table, Space } from 'antd';
import { Link } from "react-router-dom";
import { useState, useEffect} from "react";
import ReactLoading from 'react-loading';

export default function PostList() {
  const [data, setData] = useState();
  const [count, setCount] = useState();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);
  const [loading, setLoading] = useState(true)
  const [id, setId] = useState()
  useEffect(() => {
    console.log(loading)
    fetch(`http://localhost:8080/real-estate/unconfirmed`)
      .then(res => res.json())
        .then(result => {
          console.log(result.data)
          setCount(result.count)
          setData(result.data)  
          setLoading(false);
        });
  }, [])

  const onChangePagination = (page, pageSize) => {
    setPage(page);
    setPageSize(pageSize);
  }
  // const onDeleteClick = (e) => {
  //   fetch(`http://localhost:8080/real-estate/delete/${record.id}`,{
  //     method: 'DELETE',
  //   })
  //   .then(res=>res.json())
  //     .then(result => {
  //       console.log(result)
  //     })
  // }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Hành động',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          <a target='_blank' href={"http://localhost:3000/chi-tiet/" + record._id}><button className="postAccept">Xem</button></a>
          <button className="postAccept" onClick={(e) => {
            fetch(`http://localhost:8080/real-estate/update/${record._id}`,{
              headers: {
                "Content-type": "application/json"
              },
              method: "post",
              body:JSON.stringify({
                isConfirmed: true
              })
            })
            .then(res=>res.json())
              .then(result => {
                console.log(result)
          })}}>Xác nhận</button>
          <button className="postDelete" onClick={(e) => { 
            fetch(`http://localhost:8080/real-estate/delete/${record._id}`)
            .then(res=>res.json())
              .then(result => {
                console.log(result)
          })}}>Xóa</button>
        </Space>
      ),
    },
  ];

  return (
    <div className="postList">
      {loading ? (<ReactLoading type="spin" color="ffffff" height={50} width={50} className="loader" />)
      : (<Table   
        rowKey={obj => obj._id}
        dataSource={data} 
        columns={columns} 
        pagination={{ defaultPageSize: 20, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}}
        page= {page}
        pageSize = {pageSize}
        total = {count}
        onChange= {onChangePagination}
      />)}
    </div>
  );
  
}
