import "./productDetail.css";
import { React, useState, useEffect } from 'react'
import {useParams} from 'react-router-dom'

export default function ProductDetail() {
  const params = useParams()
  const [product, setProduct] = useState([])
  const [utilities, setUtilities] = useState([])

  console.log(params.productId)

  useEffect(() => {
    async function fetchData() {
        try {
            const response = await fetch(`http://localhost:8080/real-estate/get-by-id/${params.productId}`)
            const responseJson = await response.json()
            const data  = responseJson
            const listOfUtilitiesName =  data.utilsList.map(async util => {
                try {
                    const response = await  fetch(`http://localhost:8080/utilities/name/${util}`);
                    const responseJson = await response.json()
                    const name = responseJson;
                    return name;
                } catch(err){
                    console.log(err)
                }  
            })
          
            await setProduct(data)
          
            Promise.all(listOfUtilitiesName)
                .then((results) => {
                    setUtilities(results)
            }) 
        } catch (error) {
            console.log("Error:", error.message)
        }
    };
    fetchData()
  }, [5])

  console.log(product)
  
  if (product.length === 0){
    return null;
  }
  else{
    var string = product.more_description.split('\n', 10);
    return (
      <div className="productDetail">
        <div className="hogi-details">
          <div className="container">
            <div className="all-images">
              <div className="image-left">
                <img src={product.imgList[0]}></img>
              </div>
              <div className="image-right">
                <div className="image-top">
                  <div className="image-top-left">
                    <img src={product.imgList[1]}></img>
                  </div>
                  <div className="image-top-right">
                    <img src={product.imgList[2]}></img>
                  </div>
                </div>
                <div className="image-bottom">
                  <div className="image-bottom-left">
                    <img src={product.imgList[3]}></img>
                  </div>
                  <div className="image-bottom-right">
                    <img src={product.imgList[4]}></img>
                  </div>
                </div>
              </div>
            </div>
            <div className="infor-property">
              <div className="breadcrumb-scroll">
                <ul className="breadcrumb clearfix">
                  <li className="breadcrumb-item">
                    <a>Hogi</a>
                  </li>
                  <li className="breadcrumb-item">
                    <i className="fa fa-chevron-right"></i>
                    <a>{product.detail_address.district}</a>
                  </li>
                </ul>
              </div>
              <div className="list-name">
                <h2 className="project-name">{product.name}</h2>
              </div>
              <div className="list-basic-infor">
                <ul className="about-infor">
                  <li>
                    <div className="icon bg-selection"></div>
                    <p>{product.area.split(" ", 2)}  m<sup>2</sup></p>
                  </li>
                  <li>
                    <div className="icon bg-hotel"></div>
                    <p>{product.num_bedroom}</p>
                  </li>
                  <li>
                    <div className="icon bg-bathroom"></div>
                    <p>{product.num_wc}</p>
                  </li>
                </ul>
                <div className="about-price">
                  <strong>{product.price}</strong>
                </div>
              </div>
            </div>
            <div className="accordion" id="property-accordion">
              <div className="property-info-item">
                <div className="property-info-title">
                  <img src="/images/house.png"></img>
                  <strong>Tổng quan</strong>
                </div>
                <div className="property-info-content">
                  <div className="property-content-detail">
                    <div className="content-container">
                      <p id="paragraph_0">{string[0]}</p>
                      <p id="paragraph_1">{string[1]}</p>
                      <p id="paragraph_2">{string[2]}</p>
                      <p id="paragraph_3">{string[3]}</p>
                      <p id="paragraph_4">{string[4]}</p>
                      <p id="paragraph_5">{string[5]}</p>
                      <p id="paragraph_6">{string[6]}</p>
                      <p id="paragraph_7">{string[7]}</p>
                      <p id="paragraph_8">{string[8]}</p>
                      <p id="paragraph_9">{string[9]}</p>
                      <p id="paragraph_10">{string[10]}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="property-info-item">
                <div className="property-info-title">
                  <img src="/images/furnitures.png"></img>
                  <strong>Nội thất</strong>
                </div>
                <div className="property-info-content">
                  <div className="property-content-detail">
                    <div className="property-facility">
                      <ul className="list-unstyled">
                        {utilities.map((util) => {
                          return (<li>
                            <img src="/images/checked.png"></img>
                            <p>{util}</p>
                          </li>)
                        })}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
}
