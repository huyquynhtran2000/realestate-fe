import "./productList.css";
import "antd/dist/antd.css";
import { Pagination, Table, Space } from 'antd';
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import ReactLoading from 'react-loading';

export default function ProductList() {
  const [data, setData] = useState();
  const [count, setCount] = useState();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    fetch(`http://localhost:8080/real-estate/all-real-estate`)
      .then(res => res.json())
        .then(result => {
          console.log(result.data)
          setCount(result.count)
          setData(result.data)
          setLoading(false)
        })
  }, [])

  const onChangePagination = (page, pageSize) => {
    setPage(page);
    setPageSize(pageSize);
  }

  const productDetail = () => {
    console.log("aaaaaa");
  }

  console.log(data)

  const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
    },
    {
      title: 'WC',
      dataIndex: 'num_wc',
      key: 'num_wc',
    },
    {
      title: 'Bedroom',
      dataIndex: 'num_bedroom',
      key: 'num_bedroom',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          <a target='_blank' href={"http://localhost:3000/chi-tiet/" + record._id}>
          <button className="productListEdit">Xem</button>
          </a>
        </Space>
      ),
    },
];

  return (
    <div className="productList">
      {loading ? (<ReactLoading type="spin" color="ffffff" height={50} width={50} className="loader" />)
      : (<Table 
        dataSource={data} 
        columns={columns} 
        pagination={{ defaultPageSize: 20, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}}
        page= {page}
        pageSize = {pageSize}
        total = {count}
        onChange={onChangePagination}
      />)}
    </div>
  );
}
