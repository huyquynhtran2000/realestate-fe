import "./userList.css";
import { Table } from 'antd';
import { useState, useEffect} from "react";
import ReactLoading from 'react-loading';

export default function UserList() {
  const [data, setData] = useState();
  const [count, setCount] = useState();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(15);
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true);
    fetch(`http://localhost:8080/users`)
      .then(res => res.json())
        .then(result => {
          setCount(result.count);
          setData(result.users);
          setLoading(false);
        })
  }, [])

  const onChangePagination = (page, pageSize) => {
    setPage(page);
    setPageSize(pageSize);
  }

  
  const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Phone number',
        dataIndex: 'phoneNumber',
        key: 'phoneNumber',
    },
    {
      title: 'Address',
      dataIndex: 'fullAddress',
      key: 'fullAddress',
    },
];

  return (
    <div className="userList">
      {loading ? (<ReactLoading type="spin" color="ffffff" height={50} width={50} className="loader" />)
        : (<Table 
        dataSource={data} 
        columns={columns} 
        pagination={{ defaultPageSize: 20, showSizeChanger: true, pageSizeOptions: ['10', '15', '20']}}
        page= {page}
        pageSize = {pageSize}
        total = {count}
        onChange= {onChangePagination}
      />)}
    </div>
  );
}
