import './App.css';
import { React, useEffect, createContext, useReducer,useContext } from 'react'
import { BrowserRouter, Route, Switch, useHistory, useLocation } from 'react-router-dom'
import Client from './Client';
import Admin from './Admin';

function App() {
    const user = JSON.parse(localStorage.getItem("user"))
    console.log(user)
    const location = useLocation();
    const path = location.pathname;
    console.log(path.slice(0,5));
    if (user !== null && user.userType === 0 && path.slice(0,6)==="/admin")
    {
        return (
        <Switch>
            <Route path="/admin">
                <Admin />
            </Route>
        </Switch>
        );
    }
    return (
        <Switch>
            <Route path="/">
                <Client />
            </Route>
            <Route path="/admin">
                <Admin />
            </Route>
        </Switch>
    );

    
    // const location = useLocation();
    // const history = useHistory();
    // console.log(history)
    // if (location.pathname === "/")
    // {
    //     return (
    //         <Switch>
    //             <Route path="/">
    //                 <Client />
    //             </Route>
    //         </Switch>
    //     )
    //     }
    // else if ( location.pathname==="/admin"){
    //     return (
    //         <Switch>
    //             <Route path="/admin">
    //                 <Admin />
    //             </Route>
    //         </Switch>
    //         );
    // } else {
    //     return (null);
    // }
    
}

export default App;