import "./sidebar.css";
import {
  LineStyle,
  PermIdentity,
  Storefront,
  SupervisorAccount,
} from "@material-ui/icons";
import LogoutIcon from '@mui/icons-material/Logout';
import TextSnippetIcon from '@mui/icons-material/TextSnippet';
import { React, useState, useEffect,  useContext } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { useHistory, Link } from 'react-router-dom'
import {UserContext} from '../../Admin'

const Sidebar = () => {
  const history = useHistory()
  const history1 = useHistory()
  const { state1, dispatch1 } = useContext(UserContext)
  const [isClick, setIsClick] = useState(1)

  if (state1 === null)
  {
    return null;
  }
  else
  {
    return (
      <div className="sidebar">
        <div className="sidebarWrapper">
          <div className="sidebarMenu">
            <h3 className="sidebarTitle">Dashboard</h3>
            <ul className="sidebarList">
              <Link to="/admin" className="link">
              <li className={isClick === 1 ? "sidebarListItem active" : "sidebarListItem"} onClick={() => setIsClick(1)}>
                <LineStyle className="sidebarIcon" />
                Trang chủ
              </li>
              </Link>
              <Link to="/admin/users" className="link">
                <li className={isClick === 2 ? "sidebarListItem active" : "sidebarListItem"} onClick={() => setIsClick(2)}>
                  <PermIdentity className="sidebarIcon" />
                  Quản lý tài khoản
                </li>
              </Link>
              <Link to="/admin/products" className="link">
                <li className={isClick === 3 ? "sidebarListItem active" : "sidebarListItem"} onClick={() => setIsClick(3)}>
                  <Storefront className="sidebarIcon" />
                  Quản lý bất động sản
                </li>
              </Link>
              <Link to="/admin/postNews" className="link">
                <li className={isClick === 4 ? "sidebarListItem active" : "sidebarListItem"} onClick={() => setIsClick(4)}>
                  <TextSnippetIcon className="sidebarIcon" />
                  Quản lý tin đăng
                </li>
              </Link>
              <li className="sidebarListItem">
                <Link 
                      onClick={() => {
                          localStorage.clear()
                          dispatch1({ type: "CLEAR" })
                          history1.push('../')
                          window.location.reload(); 
                  }}
                >
                  <LogoutIcon className="sidebarIcon" />Đăng xuất
                  </Link>
                  </li>
              {/* <li className="sidebarListItem">
                <Timeline className="sidebarIcon" />
                Analytics
              </li>
              <li className="sidebarListItem">
                <TrendingUp className="sidebarIcon" />
                Sales
              </li> */}
            </ul>
          </div>
          {/* <div className="sidebarMenu">
            <h3 className="sidebarTitle">Quick Menu</h3>
            <ul className="sidebarList">
              <Link to="/users" className="link">
                <li className="sidebarListItem">
                  <PermIdentity className="sidebarIcon" />
                  Users
                </li>
              </Link>
              <Link to="/products" className="link">
                <li className="sidebarListItem">
                  <Storefront className="sidebarIcon" />
                  Products
                </li>
              </Link>
              <li className="sidebarListItem">
                <AttachMoney className="sidebarIcon" />
                Transactions
              </li>
              <li className="sidebarListItem">
                <BarChart className="sidebarIcon" />
                Reports
              </li>
            </ul>
          </div> */}
          {/* <div className="sidebarMenu">
            <h3 className="sidebarTitle">Notifications</h3>
            <ul className="sidebarList">
              <li className="sidebarListItem">
                <MailOutline className="sidebarIcon" />
                Mail
              </li>
              <li className="sidebarListItem">
                <DynamicFeed className="sidebarIcon" />
                Feedback
              </li>
              <li className="sidebarListItem">
                <ChatBubbleOutline className="sidebarIcon" />
                Messages
              </li>
            </ul>
          </div> */}
          {/* <div className="sidebarMenu">
            <h3 className="sidebarTitle">Staff</h3>
            <ul className="sidebarList">
              <li className="sidebarListItem">
                <WorkOutline className="sidebarIcon" />
                Admin
              </li>
              <li className="sidebarListItem">
                <Timeline className="sidebarIcon" />
                Analytics
              </li>
              <li className="sidebarListItem">
                <Report className="sidebarIcon" />
                Reports
              </li>
            </ul>
          </div> */}
        </div>
      </div>
    );
    }
}

export default Sidebar;