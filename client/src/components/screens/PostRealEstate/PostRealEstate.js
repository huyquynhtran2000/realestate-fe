import './PostRealEstate.css'
import {
    PostFilter, PostFilter1, PostFilterWard1, PostFilterWard2,
    PostFilterWard3, PostFilterWard4, PostFilterWard5, PostFilterWard6,
    PostFilterWard7, PostFilterWard8, PostFilterWard9, PostFilterWard10,
    PostFilterWard11, PostFilterWard12, PostFilterWard13, PostFilterWard14,
    PostFilterWard15, PostFilterWard16, PostFilterWard17, PostFilterWard18,
    PostFilterWard19, PostFilterWard20, PostFilterWard21, PostFilterWard22,
} from '../../Filter/PostFilter'
import { React, useState, useEffect, useContext, createContext, useReducer } from 'react'
import _ from 'lodash';
import API from '../../../services';
import { Progress } from 'reactstrap';
import uplodIcon from '../../../images/upload-image.png';



const PostRealEstate = () => {
    const [address, setAddress] = useState(null)
    const [shortAddress, setShortAddress] = useState(null)
    const [district, setDistrict] = useState(null)
    const [ward, setWard] = useState(null)
    const [city, setCity] = useState(null)
    const [transaction, setTransaction] = useState(null)
    const [category, setCategory] = useState()
    const [name, setName] = useState(null)
    const [more_description, setDescription] = useState(null)
    const [price, setPrice] = useState(0)
    const [area, setArea] = useState(0)
    const [bedroom, setBedroom] = useState(0)
    const [wc, setWC] = useState(0)

    //hàm set giá trị cho biến sau sự kiện
    function onChangeInputProvince(value) {
        console.log(value.value)
        setCity(value.value)
    }
    //hàm set giá trị cho biến sau sự kiện
    function onChangeInputDistrict(value) {
        console.log(value.value)
        setDistrict(value.value)
    }
    //hàm set giá trị cho biến sau sự kiện
    function onChangeInputWard(value) {
        console.log(value.value)
        setWard(value.value)
    }
    //hàm set giá trị cho biến sau sự kiện
    function onChangeTransaction(value) {
        console.log(value)
        setTransaction(value)
    }

    useEffect(() => {
        fetch('/categories/get-id-by-type/apartments_for_sale', {
        }).then(res => res.json())
            .then(result => {
                setCategory(result)
            })
    }, [])
    //hàm set giá trị cho biến sau sự kiện
    async function onChangeCategory(value) {
        if (value == 'canho' && transaction == 'muaban')
        {
            const response = await(await fetch('/categories/get-id-by-type/apartments_for_sale'));
            const responseJson = await response.json()
            setCategory(responseJson);
        }
        else if (value == "canho" && transaction == 'chothue')
        {
            const response = await(await fetch('/categories/get-id-by-type/apartments_for_rent'));
            const responseJson = await response.json()
            setCategory(responseJson);
        }
        else if (value == 'nha' && transaction == 'muaban')
        {
            const response = await(await fetch('/categories/get-id-by-type/townhouses_for_sale'));
            const responseJson = await response.json()
            setCategory(responseJson);
        }
        else if (value == "nha" && transaction == 'chothue')
        {
            const response = await(await fetch('/categories/get-id-by-type/townhouses_for_rent'));
            const responseJson = await response.json()
            setCategory(responseJson);
        }
    }

    const [isUploding, setUploding] = useState(false);
    const [uploadedImgs, setUplodedImgs] = useState([]);
    const [uploadProgress, setProgress] = useState(0);

    const handleChange = async (e) => {
        let { files } = e.target;

        let formData = new FormData();
        _.forEach(files, file => {
            formData.append('image', file);
        });

        setUploding(true);
        let { data } = await API.post("http://localhost:8080/file/upload",  formData, {
            onUploadProgress: ({ loaded, total }) => {
                let progress = ((loaded / total) * 100).toFixed(2);
                setProgress(progress);
            }
        });
        console.log(data.url)

        uploadedImgs.push(data.url)
        setUploding(false);
    }

    const onSubmitForm = async (e) => {
        e.preventDefault();
        console.log(uploadedImgs)
        const response = await fetch("http://localhost:8080/real-estate/post", {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwt'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'name': name,
                'id_category': category,
                'area': area,
                'area_by_num': area,
                'num_bedroom': bedroom,
                'num_wc': wc,
                'full_address': address,
                'detail_address': {
                    'ward': ward,
                    'district': district,
                    'city': city
                },
                'price': price,
                'price_by_num': price,
                'more_description': more_description,
                'imagesUrl': uploadedImgs,
            })}
        )

        const responseJson = await response.json()
        console.log(responseJson)
    }

    return (
        <main>
            <div id="breadcrumb" >
                <ol className="clearfix" >
                    <li>Trang chủ</li>
                    
                    <li>Đăng tin mới</li>
                </ol>
            </div>
            <div className="row">
                <div className="offset-md-3 col-md-6 mt-3">
                    <h1 className="page-h1 font-weight-bold mb-5">Đăng tin mới</h1>
                    <div id="addnew_post_wrap" className="addnew_post_wrap">
                        <form className="form js-form-submit-data" noValidate="novalidate" onSubmit={onSubmitForm}>
                        <section className="section">
                                <div className="section-header">
                                    <span className="section-title">Loại giao dịch</span>
                                </div>
                                <div className="section-content">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for className="control-label">Loại giao dịch</label>
                                                <select className="form-control valid" aria-invalid="false" onChange={(e) =>onChangeTransaction(e.target.value)}>
                                                    <option value="muaban">Mua bán</option>
                                                    <option value="chothue">Cho thuê</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for className="control-label">Loại bất động sản</label>
                                                <select name="estate" className="form-control valid" aria-required="true" aria-invalid="false" onChange={(e) =>onChangeCategory(e.target.value)}>
                                                    <option value="canho">Căn hộ</option>,
                                                    <option value="nha">Nhà</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="section">
                                <div className="section-header">
                                    <span className="section-title">Khu vực</span>
                                </div>
                                <div className="section-content">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="select_tinhthanhpho" className="control-label-1">Tỉnh/Thành phố</label>
                                                <div className="js-form-group-tinhthanh form-control-1">
                                                    <ul className="select-province" id="sale">
                                                        <li>
                                                            <PostFilter onChange={onChangeInputProvince}/>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="select_quanhuyen" className="control-label-1">Quận/Huyện</label>
                                                <div className="js-form-group-quanhuyen form-control-1">
                                                    <ul className="select-province" id="sale">
                                                        <li>
                                                            <PostFilter1 onChange={onChangeInputDistrict}/>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="select_phuongxa" className="control-label-1">Phường/Xã</label>
                                                <div className="js-form-group-phuongxa form-control-1">
                                                    <ul className="select-province" id="sale">
                                                        <li>
                                                        {(() => {
                                                                switch (district) {
                                                                    case 'quận 1':
                                                                        return <PostFilterWard1 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'thành phố thủ đức':
                                                                        return <PostFilterWard2 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 3':
                                                                        return <PostFilterWard3 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 4':
                                                                        return <PostFilterWard4 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 5':
                                                                        return <PostFilterWard5 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 6':
                                                                        return <PostFilterWard6 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 7':
                                                                        return <PostFilterWard7 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 8':
                                                                        return <PostFilterWard8 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 10':
                                                                        return <PostFilterWard9 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 11':
                                                                        return <PostFilterWard10 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận 12':
                                                                        return <PostFilterWard11 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận bình thạnh':
                                                                        return <PostFilterWard12 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận bình tân':
                                                                        return <PostFilterWard13 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận gò vấp':
                                                                        return <PostFilterWard14 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận phú nhuận':
                                                                        return <PostFilterWard15 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận tân bình':
                                                                        return <PostFilterWard16 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận tân phú':
                                                                        return <PostFilterWard17 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'huyện bình chánh':
                                                                        return <PostFilterWard18 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'huyện cần giờ':
                                                                        return <PostFilterWard19 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận củ chi':
                                                                        return <PostFilterWard20 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận hóc môn':
                                                                        return <PostFilterWard21 onChange={onChangeInputWard} />
                                                                        
                                                                    case 'quận nhà bè':
                                                                        return <PostFilterWard22 onChange={onChangeInputWard} />
                                                                        
                                                                    default: 
                                                                        break;
                                                            }
                                                        })()}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="select_sonha" className="control-label">Số nhà - Đường/Phố</label>
                                                <input className="form-control-1" type="text" placeholder="Ví dụ: 150 - Đường 100" onChange={(e)=>setShortAddress(e.target.value)}/>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="select_sonha" className="control-label">Địa chỉ chính xác</label>
                                                <input className="form-control-1" type="text" placeholder="Ví dụ: 150 Trần Não, Quận 2, Tp.HCM" onChange={(e)=>setAddress(e.target.value)}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="section">
                                <div className="section-header">
                                    <span className="section-title">Thông tin mô tả</span>
                                </div>
                                <div className="section-content">
                                    <div className="form-group">
                                        <label for="title" className="control-label">Tiêu đề</label>
                                        <textarea name="post_title" id="title" rows="2" className="form-control js-title form-control" aria-required="false" aria-invalid="false" onChange={(e)=>setName(e.target.value)}></textarea>
                                        <span class="form-text text-muted">Tiêu đề ngắn gọn dễ hiểu, không được dài quá 100 ký tự.</span>
                                    </div>
                                    <div className="form-group">
                                        <label for="content" className="control-label">Nội dung mô tả</label>
                                        <textarea name="description" id="editor" rows="5" className="form-control" aria-required="true" aria-invalid="false" onChange={(e)=>setDescription(e.target.value)}></textarea>
                                        <span className="form-text text-muted">Nội dung mô tả tối thiểu 150 kí tự và không được dài quá 5000 ký tự.</span>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="control-label">Giá bán</label>
                                                <div className="input-group">
                                                    <input pattern="[0-9.]+" type="text" name="price" id="input_price" autocomplete="off" placeholder="Nhập giá" className="form-control input-text" aria-required="true" aria-invalid="false" onChange={(e)=>setPrice(e.target.value)}/>
                                                    <div className="input-group-append">
                                                        <span className="input-group-text">đồng</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="control-label">Diện tích</label>
                                            <div className="input-group">
                                                    <input pattern="[0-9.]+" type="text" name="acreage" id="dientich" autocomplete="off" placeholder="Nhập diện tích bất động sản" className="form-control input-text" aria-required="true" aria-invalid="false" onChange={(e)=>setArea(e.target.value)}/>
                                                    <div className="input-group-append">
                                                        <span className="input-group-text">m<sup>2</sup></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="control-label">Số phòng ngủ</label>
                                                <div className="input-group">
                                                    <input type="text" name="so_phong_ngu" id="so_phong_ngu" className="form-control number js-number-dien-tich input-text" onChange={(e)=>setBedroom(e.target.value)}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label className="control-label">Số toilet</label>
                                                <div className="input-group">
                                                    <input type="text" name="so_toilet" id="so_toilet" className="form-control number js-number-dien-tich input-text" onChange={(e)=>setWC(e.target.value)}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="section">
                                <div class="section-header">
                                    <span class="section-title">Hình ảnh</span>
                                </div>
                                <div class="section-content">
                                    <div className="form-group">
                                        <div className="d-flex">
                                            <div className="d-flex flex-grow-1 justify-content-center align-items-center">
                                                <div className="file-uploader-mask d-flex justify-content-center align-items-center">
                                                    <img className="file-uploader-icon" src={uplodIcon} alt="Upload-Icon" />
                                                </div>
                                                <input multiple className="file-input" type="file" id="multi-uploader" onChange={handleChange} />
                                                
                                            </div>
                                            {
                                                isUploding ? (
                                                    <div className="flex-grow-2 px-2">
                                                        <div className="text-center">{uploadProgress}%</div>
                                                        <Progress value={uploadProgress} />
                                                    </div>
                                                ) : null
                                            }
                                        </div>
                                        <div className="d-flex flex-wrap mt-8">
                                            {
                                                uploadedImgs && !isUploding ? (
                                                    uploadedImgs.map(uploadedImg => (
                                                        <img src={uploadedImg} key={uploadedImg} alt="UploadedImage" className="img-thumbnail img-fluid uploaded-img mr-2" />
                                                    ))
                                                ) : null
                                            }
                                        </div>
                                        <label htmlFor="multi-uploader" className="text-primary font-weight-bold">Chọn ảnh</label>
                                    </div>
                                </div>
                            </section>
                            <div id="dashboard-bottom-action">
                                <div class="d-flex mt-5">
                                    <button type="submit" class="btn btn-primary btn-lg d-flex btn-block align-items-center justify-content-center js_btn_form_next_step_complete">Tiếp tục</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </main>
    )
}

export default PostRealEstate;