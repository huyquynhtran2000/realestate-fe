import React, { useState } from 'react';
import Select from 'react-select';
import './Mainfilter.css'

const options1 = [
  { value: 'nhà', label: 'Nhà' },
  { value: 'căn hộ', label: 'Căn hộ' },
];
const options = [
  { value: 'Quận 1', label: 'Quận 1' },
  { value: 'Quận 2', label: 'Quận 2' },
  { value: 'Quận 3', label: 'Quận 3' },
  { value: 'Quận 4', label: 'Quận 4' },
  { value: 'Quận 5', label: 'Quận 5' },
  { value: 'Quận 6', label: 'Quận 6' },
  { value: 'Quận 7', label: 'Quận 7' },
  { value: 'Quận 8', label: 'Quận 8' },
  { value: 'Quận 9', label: 'Quận 9' },
  { value: 'Quận 10', label: 'Quận 10' },
  { value: 'Quận 11', label: 'Quận 11' },
  { value: 'Quận 12', label: 'Quận 12' },
  { value: 'Quận Thủ Đức', label: 'Quận Thủ Đức' },
  { value: 'Quận Bình Thạnh', label: 'Quận Bình Thạnh' },
  { value: 'Quận Gò Vấp', label: 'Quận Gò Vấp' },
  { value: 'Quận Phú Nhuận', label: 'Quận Phú Nhuận' },
  { value: 'Quận Tân Phú', label: 'Quận Tân Phú' },
  { value: 'Quận Bình Tân', label: 'Quận Bình Tân' },
  { value: 'Quận Tân Bình', label: 'Quận Tân Bình' },
  { value: 'Huyện Nhà Bè', label: 'Huyện Nhà Bè' },
  { value: 'Huyện Bình Chánh', label: 'Huyện Bình Chánh' },
  { value: 'Huyện Hóc Môn', label: 'Huyện Hóc Môn' },
  { value: 'Huyện Cần Giờ', label: 'Huyện Cần Giờ' },
  { value: 'Huyện Củ Chi', label: 'Huyện Củ Chi' },
];

const options2 = [
  { value:['1000000000','3000000000'] , label: '1 tỷ - 3 tỷ' },
  { value:['3000000000','5000000000'], label: '3 tỷ - 5 tỷ' },
  { value:['5000000000','7000000000'], label: '5 tỷ - 7 tỷ' },
  { value:['7000000000','10000000000'], label: '7 tỷ - 10 tỷ' },
  { value:['10000000000','15000000000'], label: '10 tỷ - 15 tỷ' },
  { value:['15000000000','20000000000'], label: '15 tỷ - 20 tỷ' },
  { value:['20000000000','500000000000'], label: 'trên 20 tỷ' },
];

const options3 = [
  { value:['1000000','3000000'], label: '1 triệu - 3 triệu' },
  { value:['3000000','5000000'], label: '3 triệu - 5 triệu' },
  { value:['5000000','7000000'], label: '5 triệu - 7 triệu' },
  { value:['7000000','10000000'], label: '7 triệu - 10 triệu' },
  { value:['10000000','20000000'], label: '10 triệu - 20 triệu' },
  { value:['20000000','30000000'], label: '20 triệu - 30 triệu' },
  { value:['30000000','50000000'], label: '30 triệu - 50 triệu' },
  { value:['50000000','500000000'], label: 'trên 50 triệu' },
];


export function Mainfilter({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-one">
      <Select
        placeholder="Hồ Chí Minh"
        defaultValue={selectedOption}
        onChange={onChange}
        options={options}
        className="filter-one-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

export function Mainfilter1({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-two">
      <Select
        placeholder="Loại bất động sản"
        defaultValue={selectedOption}
        onChange={onChange}
        options={options1}
        className="filter-two-select"
        classNamePrefix="react-select-two"
      />
    </div>
  );
}

export function Mainfilter2({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-three">
      <Select
        placeholder="Giá mua"
        defaultValue={selectedOption}
        onChange={onChange}
        options={options2}
        className="filter-three-select"
        classNamePrefix="react-select-three"
      />
    </div>
  );
}

export function Mainfilter3({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-three">
      <Select
        placeholder="Giá thuê"
        defaultValue={selectedOption}
        onChange={onChange}
        options={options3}
        className="filter-three-select"
        classNamePrefix="react-select-three"
      />
    </div>
  );
}