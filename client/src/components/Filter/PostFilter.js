import React, { useState } from 'react';
import Select from 'react-select';
import './PostFilter.css'

const options = [
  { value: 'hồ chí minh', label: 'Hồ Chí Minh' },
];

export function PostFilter({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-province">
      <Select
        placeholder="Chọn tỉnh thành phố"
        defaultValue={selectedOption}
        onChange={onChange}
        options={options}
        className="filter-province-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}


const options1 = [
  { value: 'quận 1', label: 'Quận 1' },
  { value: 'quận 3', label: 'Quận 3' },
  { value: 'quận 4', label: 'Quận 4' },
  { value: 'quận 5', label: 'Quận 5' },
  { value: 'quận 6', label: 'Quận 6' },
  { value: 'quận 7', label: 'Quận 7' },
  { value: 'quận 8', label: 'Quận 8' },
  { value: 'quận 10', label: 'Quận 10' },
  { value: 'quận 11', label: 'Quận 11' },
  { value: 'quận 12', label: 'Quận 12' },
  { value: 'thành phố thủ đức', label: 'Thành phố Thủ Đức' },
  { value: 'quận bình thạnh', label: 'Quận Bình Thạnh' },
  { value: 'quận gò vấp', label: 'Quận Gò Vấp' },
  { value: 'quận phú nhuận', label: 'Quận Phú Nhuận' },
  { value: 'quận tân phú', label: 'Quận Tân Phú' },
  { value: 'quận bình tân', label: 'Quận Bình Tân' },
  { value: 'quận tân bình', label: 'Quận Tân Bình' },
  { value: 'huyện nhà bè', label: 'Huyện Nhà Bè' },
  { value: 'huyện bình chánh', label: 'Huyện Bình Chánh' },
  { value: 'huyện hóc môn', label: 'Huyện Hóc Môn' },
  { value: 'huyện cần giờ', label: 'Huyện Cần Giờ' },
  { value: 'huyện củ chi', label: 'Huyện Củ Chi' },
];

export function PostFilter1({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-district">
      <Select
        placeholder="Chọn quận/huyện"
        defaultValue={selectedOption}
        onChange={onChange}
        options={options1}
        className="filter-district-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}


const optionsWard1 = [
  { value: 'phường tân định', label: 'Phường Tân Định' },
  { value: 'phường đa kao', label: 'Phường Đa Kao' },
  { value: 'phường bến nghé', label: 'Phường Bến Nghé' },
  { value: 'phường bến thành', label: 'Phường Bến Thành' },
  { value: 'phường nguyễn thái bình', label: 'Phường Nguyễn Thái Bình' },
  { value: 'phường phạm ngũ lão', label: 'Phường Phạm Ngũ Lão' },
  { value: 'phường cầu ông lãnh', label: 'Phường Cầu Ông Lãnh' },
  { value: 'phường cô giang', label: 'Phường Cô Giang' },
  { value: 'phường nguyễn cư trinh', label: 'Phường Nguyễn Cư Trinh' },
  { value: 'phường cầu kho', label: 'Phường Cầu Kho' },
];

export function PostFilterWard1({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard1}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard2 = [
  { value: 'phường linh xuân', label: 'Phường Linh Xuân' },
  { value: 'phường bình chiểu', label: 'Phường Bình Chiểu' },
  { value: 'phường linh trung', label: 'Phường Linh Trung' },
  { value: 'phường tam bình', label: 'Phường Tam Bình' },
  { value: 'phường tam phú', label: 'Phường Tam Phú' },
  { value: 'phường hiệp bình phước', label: 'Phường Hiệp Bình Phước' },
  { value: 'phường hiệp bình chánh', label: 'Phường Hiệp Bình Chánh' },
  { value: 'phường linh chiểu', label: 'Phường Linh Chiểu' },
  { value: 'phường linh tây', label: 'Phường Linh Tây' },
  { value: 'phường linh đông', label: 'Phường Linh Đông' },
  { value: 'phường bình thọ', label: 'Phường Bình Thọ' },
  { value: 'phường trường thọ', label: 'Phường Trường Thọ' },
  { value: 'phường long bình', label: 'Phường Long Bình' },
  { value: 'phường long thạnh mỹ', label: 'Phường Long Thạnh Mỹ' },
  { value: 'phường tân phú', label: 'Phường Tân Phú' },
  { value: 'phường hiệp phú', label: 'Phường Hiệp Phú' },
  { value: 'phường tăng nhơn phú A', label: 'Phường Tăng Nhơn Phú A' },
  { value: 'phường tăng nhơn phú B', label: 'Phường Tăng Nhơn Phú B' },
  { value: 'phường phước long A', label: 'Phường Phước Long A' },
  { value: 'phường phước long B', label: 'Phường Phước Long B' },
  { value: 'phường trường thạnh', label: 'Phường Trường Thạnh' },
  { value: 'phường long phước', label: 'Phường Long Phước' },
  { value: 'phường long trường', label: 'Phường Long Trường' },
  { value: 'phường phước bình', label: 'Phường Phước Bình' },
  { value: 'phường phú hữu', label: 'Phường Phú Hữu' },
  { value: 'phường thảo điền', label: 'Phường Thảo Điền' },
  { value: 'phường an phú', label: 'Phường An Phú' },
  { value: 'phường an khánh', label: 'Phường An Khánh' },
  { value: 'phường bình trưng đông', label: 'Phường Bình Trưng Đông' },
  { value: 'phường bình trưng tây', label: 'Phường Bình Trưng Tây' },
  { value: 'phường cát lái', label: 'Phường Cát Lái' },
  { value: 'phường thạnh mỹ lợi', label: 'Phường Thạnh Mỹ Lợi' },
  { value: 'phường an lợi đông', label: 'Phường An Lợi Đông' },
  { value: 'phường thủ thiêm', label: 'Phường Thủ Thiêm' },
];

export function PostFilterWard2({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard2}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard3 = [
  { value: 'phường võ thị sáu', label: 'Phường Võ Thị Sáu' },
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
];

export function PostFilterWard3({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard3}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard4 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
  { value: 'phường 16', label: 'Phường 16' },
  { value: 'phường 18', label: 'Phường 18' },
];

export function PostFilterWard4({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard4}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard5 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
];

export function PostFilterWard5({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard5}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard6 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
];

export function PostFilterWard6({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard6}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard7 = [
  { value: 'phường tân thuận đông', label: 'Phường Tân Thuận Đông' },
  { value: 'phường tân thuận tây', label: 'Phường Tân Thuận Tây' },
  { value: 'phường tân kiểng', label: 'Phường Tân Kiểng' },
  { value: 'phường tân hưng', label: 'Phường Tân Hưng' },
  { value: 'phường bình thuận', label: 'Phường Bình Thuận' },
  { value: 'phường tân quy', label: 'Phường Tân Quy' },
  { value: 'phường phú thuận', label: 'Phường Phú Thuận' },
  { value: 'phường tân phú', label: 'Phường Tân Phú' },
  { value: 'phường tân phong', label: 'Phường Tân Phong' },
  { value: 'phường phú mỹ', label: 'Phường Phú Mỹ' },
];

export function PostFilterWard7({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard7}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard8 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
  { value: 'phường 16', label: 'Phường 16' },
];

export function PostFilterWard8({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard8}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard9 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
];

export function PostFilterWard9({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard9}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

const optionsWard10 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
  { value: 'phường 16', label: 'Phường 16' },
];

export function PostFilterWard10({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard10}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//quan 12
const optionsWard11 = [
  { value: 'phường thạnh xuân', label: 'Phường Thạnh Xuân' },
  { value: 'phường thạnh lộc', label: 'Phường Thạnh Lộc' },
  { value: 'phường hiệp thành', label: 'Phường Hiệp Thành' },
  { value: 'phường thới an', label: 'Phường Thới An' },
  { value: 'phường tân chánh hiệp', label: 'Phường Tân Chánh Hiệp' },
  { value: 'phường an phú đông', label: 'Phường An Phú Đông' },
  { value: 'phường tân thới hiệp', label: 'Phường Tân Thới Hiệp' },
  { value: 'phường trung mỹ tây', label: 'Phường Trung Mỹ Tây' },
  { value: 'phường tân hưng thuận', label: 'Phường Tân Hưng Thuận' },
  { value: 'phường đông hưng thuận', label: 'Phường Đông Hưng Thuận' },
  { value: 'phường tân thới nhất', label: '	Phường Tân Thới Nhất' },
];

export function PostFilterWard11({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard11}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//binh thanh
const optionsWard12 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
  { value: 'phường 17', label: 'Phường 17' },
  { value: 'phường 19', label: 'Phường 19' },
  { value: 'phường 21', label: 'Phường 21' },
  { value: 'phường 22', label: 'Phường 22' },
  { value: 'phường 24', label: 'Phường 24' },
  { value: 'phường 25', label: 'Phường 25' },
  { value: 'phường 26', label: 'Phường 26' },
  { value: 'phường 27', label: 'Phường 27' },
  { value: 'phường 28', label: 'Phường 28' },
];

export function PostFilterWard12({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard12}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//binh tan
const optionsWard13 = [
  { value: 'phường bình hưng hòa', label: 'Phường Bình Hưng Hòa' },
  { value: 'phường bình hưng hòa A', label: 'Phường Bình Hưng Hòa A' },
  { value: 'phường bình hưng hòa B', label: 'Phường Bình Hưng Hòa B' },
  { value: 'phường bình trị đông', label: 'Phường Bình Trị Đông' },
  { value: 'phường bình trị đông A', label: 'Phường Bình Trị Đông A' },
  { value: 'phường bình trị đông B', label: 'Phường Bình Trị Đông B' },
  { value: 'phường tân tạo', label: 'Phường Tân Tạo' },
  { value: 'phường tân tạo A', label: 'Phường Tân Tạo A' },
  { value: 'phường an lạc', label: 'Phường An Lạc' },
  { value: 'phường an lạc A', label: 'Phường An Lạc A' },
];

export function PostFilterWard13({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard13}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//go vap
const optionsWard14 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
  { value: 'phường 16', label: 'Phường 16' },
  { value: 'phường 17', label: 'Phường 17' },
];

export function PostFilterWard14({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard14}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//phú nhận
const optionsWard15 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 15', label: 'Phường 15' },
  { value: 'phường 17', label: 'Phường 17' },
];

export function PostFilterWard15({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard15}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//tan binh
const optionsWard16 = [
  { value: 'phường 01', label: 'Phường 01' },
  { value: 'phường 02', label: 'Phường 02' },
  { value: 'phường 03', label: 'Phường 03' },
  { value: 'phường 04', label: 'Phường 04' },
  { value: 'phường 05', label: 'Phường 05' },
  { value: 'phường 06', label: 'Phường 06' },
  { value: 'phường 07', label: 'Phường 07' },
  { value: 'phường 08', label: 'Phường 08' },
  { value: 'phường 09', label: 'Phường 09' },
  { value: 'phường 10', label: 'Phường 10' },
  { value: 'phường 11', label: 'Phường 11' },
  { value: 'phường 12', label: 'Phường 12' },
  { value: 'phường 13', label: 'Phường 13' },
  { value: 'phường 14', label: 'Phường 14' },
  { value: 'phường 15', label: 'Phường 15' },
];

export function PostFilterWard16({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard16}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//tan phu
const optionsWard17 = [
  { value: 'phường tân sơn nhì', label: 'Phường Tân Sơn Nhì' },
  { value: 'phường tây thạnh', label: 'Phường Tây Thạnh' },
  { value: 'phường sơn kỳ', label: 'Phường Sơn Kỳ' },
  { value: 'phường tân quý', label: 'Phường Tân Quý' },
  { value: 'phường tân thành', label: 'Phường Tân Thành' },
  { value: 'phường phú thọ hòa', label: 'Phường Phú Thọ Hòa' },
  { value: 'phường phú thạnh', label: 'Phường Phú Thạnh' },
  { value: 'phường phú trung', label: 'Phường Phú Trung' },
  { value: 'phường hòa thạnh', label: 'Phường Hòa Thạnh' },
  { value: 'phường hiệp tân', label: 'Phường Hiệp Tân' },
  { value: 'phường tân thới hòa', label: 'Phường Tân Thới Hòa' },
];

export function PostFilterWard17({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard17}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//binh chanh
const optionsWard18 = [
  { value: 'thị trấn tân túc', label: 'Thị trấn Tân Túc' },
  { value: 'xã phạm văn hai', label: 'Xã Phạm Văn Hai' },
  { value: 'xã vĩnh lộc A', label: 'Xã Vĩnh Lộc A' },
  { value: 'xã vĩnh lộc B', label: 'Xã Vĩnh Lộc B' },
  { value: 'xã bình lợi', label: 'Xã Bình Lợi' },
  { value: 'xã lê minh xuân', label: 'Xã Lê Minh Xuân' },
  { value: 'xã tân nhựt', label: 'Xã Tân Nhựt' },
  { value: 'xã tân kiên', label: 'Xã Tân Kiên' },
  { value: 'xã bình hưng', label: 'Xã Bình Hưng' },
  { value: 'xã phong phú', label: 'Xã Phong Phú' },
  { value: 'xã an phú tây', label: 'Xã An Phú Tây' },
  { value: 'xã hưng long', label: 'Xã Hưng Long' },
  { value: 'xã đa phước', label: 'Xã Đa Phước' },
  { value: 'xã tân quý tây', label: 'Xã Tân Quý Tây' },
  { value: 'xã bình chánh', label: 'Xã Bình Chánh' },
  { value: 'xã quy đức', label: 'Xã Quy Đức' },
];

export function PostFilterWard18({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard18}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//can gio
const optionsWard19 = [
  { value: 'thị trấn cần thạnh', label: 'Thị trấn Cần Thạnh' },
  { value: 'xã bình khánh', label: 'Xã Bình Khánh' },
  { value: 'xã tam thôn hiệp', label: 'Xã Tam Thôn Hiệp' },
  { value: 'xã an thới đông', label: 'Xã An Thới Đông' },
  { value: 'xã thạnh an', label: 'Xã Thạnh An' },
  { value: 'xã long hòa', label: 'Xã Long Hòa' },
  { value: 'xã lý nhơn', label: 'Xã Lý Nhơn' },
];

export function PostFilterWard19({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard19}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//cu chi
const optionsWard20 = [
  { value: 'thị trấn củ chi', label: 'Thị trấn Củ Chi' },
  { value: 'xã phú mỹ hưng', label: 'Xã Phú Mỹ Hưng' },
  { value: 'xã an phú', label: 'Xã An Phú' },
  { value: 'xã trung lập thượng', label: 'Xã Trung Lập Thượng' },
  { value: 'xã an nhơn tây', label: 'Xã An Nhơn Tây' },
  { value: 'xã nhuận đức', label: 'Xã Nhuận Đức' },
  { value: 'xã phạm văn cội', label: 'Xã Phạm Văn Cội' },
  { value: 'xã phú hòa đông', label: 'Xã Phú Hòa Đông' },
  { value: 'xã trung lập hạ', label: 'Xã Trung Lập Hạ' },
  { value: 'xã trung an', label: 'Xã Trung An' },
  { value: 'xã phước thạnh', label: 'Xã Phước Thạnh' },
  { value: 'xã phước hiệp', label: 'Xã Phước Hiệp' },
  { value: 'xã tân an hội', label: 'Xã Tân An Hội' },
  { value: 'xã phước vĩnh an', label: 'Xã Phước Vĩnh An' },
  { value: 'xã thái mỹ', label: 'Xã Thái Mỹ' },
  { value: 'xã tân thạnh tây', label: 'Xã Tân Thạnh Tây' },
  { value: 'xã hòa phú', label: 'Xã Hòa Phú' },
  { value: 'xã tân thạnh đông', label: 'Xã Tân Thạnh Đông' },
  { value: 'xã bình mỹ', label: 'Xã Bình Mỹ' },
  { value: 'xã tân phú trung', label: 'Xã Tân Phú Trung' },
  { value: 'xã tân thông hội', label: 'Xã Tân Thông Hội' },
];

export function PostFilterWard20({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard20}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//hoc mon
const optionsWard21 = [
  { value: 'thị trấn hóc môn', label: 'Thị trấn Hóc Môn' },
  { value: 'xã tân hiệp', label: 'Xã Tân Hiệp' },
  { value: 'xã nhị bình', label: 'Xã Nhị Bình' },
  { value: 'xã đông thạnh', label: 'Xã Đông Thạnh' },
  { value: 'xã tân thới nhì', label: 'Xã Tân Thới Nhì' },
  { value: 'xã thới tam thôn', label: 'Xã Thới Tam Thôn' },
  { value: 'xã xuân thới sơn', label: 'Xã Xuân Thới Sơn' },
  { value: 'xã tân xuân', label: 'Xã Tân Xuân' },
  { value: 'xã xuân thới đông', label: 'Xã Xuân Thới Đông' },
  { value: 'xã trung chánh', label: 'Xã Trung Chánh' },
  { value: 'xã xuân thới thượng', label: 'Xã Xuân Thới Thượng' },
  { value: 'xã bà điểm', label: 'Xã Bà Điểm' },
];

export function PostFilterWard21({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard21}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}

//nha be
const optionsWard22 = [
  { value: 'thị trấn nhà bè', label: 'Thị trấn Nhà Bè' },
  { value: 'xã phước kiển', label: 'Xã Phước Kiển' },
  { value: 'xã phước lộc', label: 'Xã Phước Lộc' },
  { value: 'xã nhơn đức', label: 'Xã Nhơn Đức' },
  { value: 'xã phú xuân', label: 'Xã Phú Xuân' },
  { value: 'xã long thới', label: 'Xã Long Thới' },
  { value: 'xã hiệp phước', label: 'Xã Hiệp Phước' },
];

export function PostFilterWard22({onChange}) {
  const [selectedOption, setSelectedOption] = useState(null);
  return (
    <div className="filter-ward">
      <Select
        placeholder="Chọn Phường/Xã"
        defaultValue={selectedOption}
        onChange={onChange}
        options={optionsWard22}
        className="filter-ward-select"
        classNamePrefix="react-select"
      />
    </div>
  );
}
